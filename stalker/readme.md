# Stalker Official Writup

Stalker was a post incident analysis on the blockchain. I remind you the wording :

## Subject 
*The THCon ticketing operating on `0x0Eba39cF8d9C45CEf1b4d028d38e7269Fa7e580E` has been hacked recently. Try to answer some questions about how this was possible. The TH23Coin token emitted for the event was initially airdropped to a group of 3 organizators.*

![](./we_are_sorry.png)


## Step 1

### What was the address which attacked the smart contract ?

We will solve stalker using [Etherscan Goerli](https://goerli.etherscan.io/). Let's dive into the hacked smart contract on [https://goerli.etherscan.io/address/0x0Eba39cF8d9C45CEf1b4d028d38e7269Fa7e580E](https://goerli.etherscan.io/address/0x0Eba39cF8d9C45CEf1b4d028d38e7269Fa7e580E).

![](step1_1.png)

**There is clearly an address which is suspicious.** The address bought a lot of tickets and then refund them. Just after, the ticketing system is closed because someone probably used the "red panic stop everything" button (it might be an organiser, but we will see that later.)

#### Let's see the smart contract code.

The smart contract is an OpenZeppelin ERC721 based NFT. At the end of the smart contract there is the custom implementation "Golden Ticket" with 2 interresting functions:

```sol
contract GoldenTicket is ERC721, Ownable {

   // [.......]

    // to buy a ticket
    function buyTicket(uint256 donationWithTicket) public {
     // [....]
    }

    // [ .... ]
    // in case of someone can't go to THCon 2023
    function refund(uint256 tokenId) public {
        // [...]
    }

}
```

You can buy a ticket with a generous TH23Coin donation to the THCon. Also, you can refund your NFT if you can't go to the event, but you will not be refunded on your donation.

Let's take a random "buy ticket" transaction which the suspicious address made :

![](./step1_2.png)

The suspicious address seems to **have bought a ticket... for free** ! And the donation he made seems to be ... a very, very generous one (115,792,089,237,316,200,000,000,000,000,000,000,000,000,000,000,000,000,000,000 TH23Coin).


He then refunded the free ticket and got some TH23Coin for free. That's how he managed to get a lot of tokens for free.

![](./step1_3.png)

So, the answer was : `THCon23{0x89c72015dAF5c76B2FB3D41d327fc07E9EF43E20}`

## Step 2

### How many THCoin23 token has been robbed ?

Just go to the hacker address and see how much coin he got with this attack :

![](./step2_1.png)

7 transactions with 200 TH23Coin : `THCon23{1400}`.

## Step 3

### Which simple technique of attack did the attacker do ?

**The simple answer is:** look at the step 1 above. Regarding the amount of the donation, it must be something with ... integer overflow :)   : `THCon23{overflow}`

--

**The more complex answer:** we will look at the "buyticket" function of the smart contract : 

```sol
    // to buy a ticket
    function buyTicket(uint256 donationWithTicket) public {
        // save lot of gas by using unchecked on a block with math operation
        unchecked {
            require(ticketOpen, "Ticketing is not open");
            uint256 tokenRequiredToMint;
            tokenRequiredToMint = tokenAmountToMint  + donationWithTicket;
            require(
                IERC20(thcoinAddress).balanceOf(msg.sender) >= tokenRequiredToMint,
                "Insufficient balance"
            );
            IERC20(thcoinAddress).transferFrom(msg.sender, address(this), tokenRequiredToMint);
            uint256 tokenId = _tokenIds.current();
            _mint(msg.sender, tokenId);
            _tokenIds.increment();
        }
    }
```

In order to save gas (to reduce the network cost to buy a ticket), the creator of the smart contract created a unchecked block to bypass underflow / overflow verification. Then, the contract calculates the amount of TH23Coin which is the sum of the price of a ticket AND the donation associated to it. With a simple calculus you can manage to make the `tokenRequiredToMint` equals to zero : `donation = integer.maxint - priceOfATicket`.

So again, the category class of this attack is an integer overflow : `THCon23{overflow}`


## Step 4

### On which DEX platform did the hacker convert its token into USDC ?

I am really sorry 😥. I'm just writting the writup after the event and i see that the DEX DeGate reset all their testnet infrastructure on mid-march 2023 and i did the challenge on late february 2023. So it was nearly impossible to solve this step (unless a very deep analysis on the smart contract the hacker sent money to, but really difficult).

So that's nearly gessing it. We had 10 tries up to 12 at the end of the day. A simple search on some DeX testnet could give us some hints about probable testnet :

![](./step4_1.png)

The good one was **DeGate** but as i said that was nearly a guess unless very deep analysis.

The solution was : `THCon23{degate}` 

## Step 5 

### How much USDC did he get from its THCoin23 on that platform ? 

Impossible to do because of DeGate reset infrastructure on testnet. I wont go any further and i am really sorry about this incident.

## Step 6

### The hacker has probably converted its THCoin23 into --> USDC into --> ETH. Which service did the hacker use to dry its ETH ?

Thanks to the challenge architecture Step 4 and 5 wasn't needed to solve the rest of the Stalker challenge. The hacker did convert the THCoin23 which is pretty illiquid to ETH on the platform. He then retreives the "dirty" ETH directly into his wallet :

![](./step6_1.png)

a few minutes later he transfered exactly 0.1 ETH two times to the same smart contract. Let's dive into this smart contract :

![](./step6_2.png)

![](./step6_3.png)

The name of the smart contract is "TornadoProxy". It's a proxy for tornadocash service. You can access to tornadocash from different websites such as `https://tornadocash.eth.link/`. 

So the flag was : `THCon23{tornadocash}`


## Step 7

### The hacker doesn't dry correctly its ETH. What is the new clean address the hacker used to withdraw its dirty ETH ?

The security of tornadocash in ensured partly by **waiting a certain amount of time** between the deposit and the withdraw of the asset. If you do not wait sufficient other deposits you can link two different accounts even if you use tornadocash. Here, the hacker was eager to buy something with this fresh money and retreive the fund after waiting a few minutes :

![](./step7_1.png)

In the tornadoproxy contract, we just list the transaction history and retreive when the hacker deposit his funds and try the very next adress after.

Flag is : `THCon23{0x93A6D878f4d1EB8a8a183430F024e9555016250c}`

## Step 8

### What is the twitter account of the Hacker ?

Now we have the new hacker address. For him, it's a "clean" one. Let's dive into the wallet transaction history :

![](./step8_1.png)

The hacker bought a NFT on opensea with the money, we see on opensea testnet that is a "Stalker" NFT (that might be additionnal hint for step 7) :

![](./step8_2.png)

Then the NFT was bought and transferred on another address ([0x9B431800b42Ec278264b5d84f8703BAa03DdDAe6](https://goerli.etherscan.io/address/0x9b431800b42ec278264b5d84f8703baa03dddae6)): 

![](./step8_3.png)

On this address wallet, we see a ENS domain registration. ENS is stand for "Ethereum Name Service", it's like a DNS but decentralised on Ethereum :

![](./step8_4.png)

In fact, this wallet is the "official clean wallet" of the hacker. We can find on the ENS registration transaction some information about him. I used the dirty way by showing raw eth data transaction but you might find a proper testnet ENS resolver to have a nice looking about this address : 

![](./step8_5.png)


So the twitter account is @ChapJacques : `THCon23{@ChapJacques}`.

## Step 9

### The hacker discovered a flaw because someone told him the smart contract could be vulnerable. Which address can terribly be the hacker's accomplice ?

We know who is the H@ck4r, wow. Let's see his twitter account :

![](./step9_1.png)

Don't worry about the face, it has been generated on thispersonndoesnotexist.com.

We can confirm a lot of things : he is from Toulouse, where the CTF is been on, he "loves crypto", and we see another hint about step 3 about integer overflow, and a link about the NFT he bought on Opensea.

There is a strange picture he has uploaded on his account : 

![](./step9_2.png)

He has setup a "brand new" eth account, and he censored the account address. But he leaves two clues :
1. The first and last address character
2. The digest image (little image on the left).

So the accomplice might be someone involved with the THCon, maybe an organizer. Let's see the official ERC20 smart contract of the THCoin23 token ([https://goerli.etherscan.io/address/0xf46146564c9039de39ef06a9507ef90f3976dea1#code](https://goerli.etherscan.io/address/0xf46146564c9039de39ef06a9507ef90f3976dea1#code)) :

![](./step9_3.png)

There are clearly 3 organizers which received an airdrop of THCoin23 :

1. `0x130519D5d9a42e5e51D15Db613B0A281598a9499`
2. `0x7309e5e7BEdb389DB3Af97Bd951b2C59E7d7B69e`
3. `0x36E2382Bc7e8685721f0bffBcCbf613070Ef7DED`

Now you need to investigate a little bit and try to find an address which starts with a "C" and ends with a "2". I was a little bit nasty because i mined a bunch of address with this condition in order to confuse the challengers 😈.

Example here with two addresses where an organizer interacted to, but there were many and distributed among lots of address (you could find like 8 to 10 addresses like this, linked to organizers or near it) : 

![](./step9_4.png)


So you have to additionnaly confirm the address match with the "digest" image. The Organizer `0x130519D5d9a42e5e51D15Db613B0A281598a9499`  sent some tokens to `0xC42FD33E4104fD5739504a5274A1C7A596C9A992` :

![](./step9_5.png)

There were like 8 to 10 other potential addresses. But when you go to the wallet address on this particular account you will be able to see that the digest image of the wallet matches !

![](./step9_6.png)

Then you could make a link between the hacker an one of the organizer 🙂.

The last flag was : `THCon23{0x130519D5d9a42e5e51D15Db613B0A281598a9499}`.


## Thanks for all the challengers !

and sorry for the english typos 😅